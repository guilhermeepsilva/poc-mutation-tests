﻿using System;

namespace PoC.Mutation.Tests.Main
{
    /// <summary>
    /// Aplica desconto a um valor principal.
    /// </summary>
    public class ApplyDiscount
    {
        public const string AMOUNT_LESS_THAN_ZERO =
            "O valor do principal deve ser maior que zero.";
        public const string PERCENT_OF_DISCOUNT_LESS_THAN_ZERO =
            "O percentual de desconto deve ser maior que zero.";
        public const string PERCENT_OF_DISCOUNT_GREATER_THAN_ONE_HUNDRED =
            "Não é possível aplicar mais do que 100% de desconto.";

        /// <summary>
        /// Realiza cálculo de desconto dado o percentual e o valor
        /// principal.
        /// </summary>
        /// <param name="amount"> Valor principal. </param>
        /// <param name="percent"> Valor em pontos percentuais do 
        ///     desconto a ser aplicado no principal. </param>
        /// <exception cref="ArgumentException"> Quando o valor
        ///     do principal ou o valor de desconto são menores
        ///     ou iguais a zero. </exception>
        /// <returns> Valor com o desconto aplicado. </returns>
        public decimal Calculate(decimal amount, decimal percent)
        {
            if (amount <= 0)
                throw new ArgumentException(AMOUNT_LESS_THAN_ZERO);

            if (percent <= 0)
                throw new ArgumentException(PERCENT_OF_DISCOUNT_LESS_THAN_ZERO);

            if (percent > 1) 
                throw new ArgumentException(PERCENT_OF_DISCOUNT_GREATER_THAN_ONE_HUNDRED);

            return amount - amount * percent;
        }
    }
}
