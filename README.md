# README #

Uma prova de conceito (PoC) do uso da biblioteca Stryker.NET para aplicação de mutação de testes com o objetivo de enriquecer a qualidade das implementações de testes automatizados.

### Etapas de execução ###

É necessário ter a instalação do striker-cli para a execução das mutações de testes. Para realizar a instação basta executar via terminal o comando:

```
 dotnet tool install -g dotnet-stryker
```

_Pode ocorrer de o processo de instalação retornar um erro 401 (Unauthorized), semelhante ao seguinte_:

```
PS C:\Users\guilherme.silva\Documents\source\repos\poc-mutation-tests> dotnet tool install -g dotnet-stryker
C:\Program Files\dotnet\sdk\3.1.402\NuGet.targets(128,5): error : Falha ao recuperar informações sobre 'dotnet-stryker' da origem remota 'https://pkgs.dev.azure.com/meuacerto/_packaging/c72a505d-bd2f-4490-8113-1e7a006dacf4/nuget/v3/flat2/dotnet-stryker/index.json'. [C:\Users\guilherme.silva\AppData\Local\Temp\djxfjkp3.lp4\restore.csproj]
C:\Program Files\dotnet\sdk\3.1.402\NuGet.targets(128,5): error :   Response status code does not indicate success: 401 (Unauthorized). [C:\Users\guilherme.silva\AppData\Local\Temp\djxfjkp3.lp4\restore.csproj]
```

_Este erro 401 ocorre quando há um Nuget local configurado e por algum motivo há uma falha de autenticação. Neste caso, para forçar a instalação via Nuget oficial, deve-se executar o seguinte comando_:


```
dotnet tool install -g dotnet-stryker --ignore-failed-sources
```

Após a instalação do stryker-cli basta estar na pasta da solução e acionar a execução do stryker através do comando `dotnet stryker`.

Esta execução irá gerar um output na raiz do projeto de testes contendo alguns logs e um relatório HTML com o resultado da execução.

### Para saber mais ###

Para saber mais a respeito do stryker e de como a mutação de testes funciona, veja as documentações:

* [Stryker-Net](https://github.com/stryker-mutator/stryker-net/blob/master/docs/Configuration.md)
* [Stryker in Pipeline](https://github.com/stryker-mutator/stryker-net/blob/master/docs/Stryker-in-pipeline.md)
* [Mutators](https://github.com/stryker-mutator/stryker-net/blob/master/docs/Mutators.md)