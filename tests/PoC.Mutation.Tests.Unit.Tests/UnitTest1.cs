using PoC.Mutation.Tests.Main;
using System;
using Xunit;

namespace PoC.Mutation.Tests.Unit.Tests
{
    public class UnitTest1
    {
        [Theory(DisplayName = "DEVE calcular um desconto no valor principal.")]
        [InlineData(1, 1, 0)]
        [InlineData(1533.45, 0.2, 1226.76)]
        public void Calculate_ShouldCalculateADiscount(decimal amount,
            decimal percent,
            decimal expectedAmount)
        {
            var applyDiscount = new ApplyDiscount();

            decimal amountWithDiscount = applyDiscount.Calculate(amount, percent);

            Assert.Equal(expectedAmount, amountWithDiscount);
        }

        [Theory(DisplayName = "N�O DEVE calcular desconto quando o principal � menor ou igual a zero.")]
        [InlineData(-1, 1)]
        [InlineData(0, 0.2)]
        public void Calculate_WhenAmountIsLessOrEqualZero_ShoudNotCalculateADiscount(decimal amount,
            decimal percent)
        {
            var applyDiscount = new ApplyDiscount();

            ArgumentException ex = Assert
                .Throws<ArgumentException>(() => applyDiscount.Calculate(amount, percent));

            Assert.Equal(ApplyDiscount.AMOUNT_LESS_THAN_ZERO, ex.Message);
        }

        [Theory(DisplayName = "N�O DEVE calcular o desconto quando o percentual de desconto � menor ou " +
            "igual a zero.")]
        [InlineData(2155.21, 0)]
        [InlineData(1500, -1)]
        public void Calculate_WhenPercentOfDiscountIsLessOrEqualZero_ShoudNotCalculateADiscount(decimal amount,
            decimal percent)
        {
            var applyDiscount = new ApplyDiscount();

            ArgumentException ex = Assert
                .Throws<ArgumentException>(() => applyDiscount.Calculate(amount, percent));

            Assert.Equal(ApplyDiscount.PERCENT_OF_DISCOUNT_LESS_THAN_ZERO, ex.Message);
        }

        [Fact(DisplayName = "N�O DEVE calcular o desconto quando o percentual de desconto superar 100%.")]
        public void Calculate_WhenPercentOfDiscountGreaterThanOne_ShoudNotCalculateADiscount()
        {
            var applyDiscount = new ApplyDiscount();
            decimal amount = 2545.93M;
            decimal percent = 1.1M;

            ArgumentException ex = Assert
                .Throws<ArgumentException>(() => applyDiscount.Calculate(amount, percent));

            Assert.Equal(ApplyDiscount.PERCENT_OF_DISCOUNT_GREATER_THAN_ONE_HUNDRED, ex.Message);
        }
    }
}
